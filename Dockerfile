# Docker Build Stage
FROM maven:3.8.4-openjdk-8 AS build

# Build Stage
WORKDIR /opt/app

COPY ./ /opt/app
RUN mvn clean install > /opt/app/maven_build.log 2>&1

# Docker Build Stage
FROM openjdk:8-jdk-alpine

WORKDIR /opt/app

COPY --from=build /opt/app/target/*.jar app.jar

ENV PORT 8081
EXPOSE $PORT

ENTRYPOINT ["java", "-jar", "-Xmx1024M", "-Dserver.port=${PORT}", "app.jar"]
